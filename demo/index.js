function Cat(name, age) {
  this.name = name;
  this.age = age;
}

let cat1 = new Cat("tom", 2);

class Dog {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}
let dog1 = new Dog("bull", 1);
console.log(`  🚀: dog1`, dog1);
