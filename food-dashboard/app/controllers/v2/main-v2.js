import {
  layThongTinTuForm,
  onSuccess,
  renderFoodList,
} from './controller-v2.js';

let BASE_URL = 'https://63b2c99f5901da0ab36dbaed.mockapi.io/food_es6';

let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: 'GET',
  })
    .then((res) => {
      console.log(res);
      renderFoodList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchFoodList();

let deleteFood = (idFood) => {
  console.log(`  🚀: deleteFood -> idFood`, idFood);
  axios({
    // url: `${BASE_URL}/${idFood}`,
    url: BASE_URL + '/' + idFood,
    method: 'DELETE',
  })
    .then((res) => {
      onSuccess();

      // gọi lại api lấy danh sách món ăn SAU khi xoá thành công
      fetchFoodList();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.deleteFood = deleteFood;
// https://apvarun.github.io/toastify-js/

let createFood = () => {
  console.log('yes');
  let food = layThongTinTuForm();
  axios({
    url: BASE_URL,
    method: 'POST',
    data: food,
  })
    .then((res) => {
      $('#exampleModal').modal('hide');
      onSuccess();
      fetchFoodList();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.createFood = createFood;

let updateFood = () => {
  $('#exampleModal').modal('show');
};

window.updateFood = updateFood;
