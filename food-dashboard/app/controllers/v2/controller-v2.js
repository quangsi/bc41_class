export let renderFoodList = (foodArr) => {
  let contentHTML = '';
  foodArr.forEach((item) => {
    let contentTr = ` <tr>
                            <td>${item.ma}</td>
                            <td>${item.ten}</td>
                            <td>${item.loai}</td>
                            <td>${item.gia}</td>
                            <td>${item.khuyenMai}</td>
                            <td>0</td>
                            <td>${item.tinhTrang}</td>
                            <td>
                            <button
                            onclick="deleteFood(${item.ma})"
                            class="btn btn-danger">Xoá</button>

                             <button
                            onclick="updateFood(${item.ma})"
                            class="btn btn-secondary">Sửa</button>
                            </td>
                      </tr>`;

    contentHTML += contentTr;
  });
  document.getElementById('tbodyFood').innerHTML = contentHTML;
};
// anfn
/**
 * {
    "ten": "Goldstripe sardinella",
    "loai": false,
    "gia": "537.00",
    "khuyenMai": 2771,
    "hinhAnh": "https://loremflickr.com/640/480/food",
    "moTa": "Porro iste quis numquam dolor perspiciatis distinctio commodi assumenda sed.",
    "tinhTrang": true,
    "ma": "1"
}
 */

export let onSuccess = () => {
  Toastify({
    text: 'Success',
    duration: 3000,
    className: 'bg-dark text-white',
  }).showToast();
};

let onFail = () => {};

export let layThongTinTuForm = () => {
  let ma = document.getElementById('foodID').value;
  let ten = document.getElementById('tenMon').value;
  let loai = document.getElementById('loai').value;
  let gia = document.getElementById('giaMon').value;
  let khuyenMai = document.getElementById('khuyenMai').value;
  let tinhTrang = document.getElementById('tinhTrang').value;
  let hinhAnh = document.getElementById('hinhMon').value;
  let moTa = document.getElementById('moTa').value;
  return {
    ma,
    ten,
    loai,
    gia,
    khuyenMai,
    tinhTrang,
    hinhAnh,
    moTa,
  };
};
